<?php
include_once 'tripit.php';



$api_url = 'https://api.tripit.com';

session_id( 'uid' );
session_start();

if ( !empty( $_SESSION['uid'] ) ) {
	echo json_encode( array( 'status' => 'session',
			'msg'=> 'session started' ) );
}else {
	$oauth_credential = new OAuthConsumerCredential( oauth_consumer_key_trip,
		oauth_consumer_key_secret_trip );

	$tripit = new TripIt( $oauth_credential, $api_url );
	$keys = $tripit->get_request_token();

	if ( $keys['oauth_token'] != null && $keys['oauth_token_secret'] != null ) {
		$_SESSION['uid'] = uniqid( 'api_' );
		$_SESSION['oauth_request_key_trip'] = $keys['oauth_token'];
		$_SESSION['oauth_request_key_secret_trip'] = $keys['oauth_token_secret'];

		echo json_encode( array( 'status' => 'success',
				'msg'=> 'permission ok',
				'oauth_token' =>  $keys['oauth_token'] ) );
	}
}




?>
