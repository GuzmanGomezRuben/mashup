<?php

// Copyright 2008-2009 TripIt, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

$VERSION='$Id: example.php 21073 2009-12-21 21:43:20Z brian $';

include_once 'tripit.php';

session_id('uid');
session_start();

if ( empty( $_GET['url'] ) ) {

	echo json_encode( array( 'status' => 'errorURL'
			, 'msg' => 'missing URL' ) );

}else if ( empty($_SESSION['oauth_access_key_trip'])||
		empty($_SESSION['oauth_access_key_secret_trip']) ) {

		echo json_encode( array( 'status' => 'errorKeys'
				, 'msg' => 'missing access keys' ) );

	}else {
	$api_url=$_GET['url'];


	$oauth_consumer_key = oauth_consumer_key_trip;
	$oauth_consumer_secret = oauth_consumer_key_secret_trip;
	$oauth_access_token = $_SESSION['oauth_access_key_trip'];
	$oauth_access_token_secret = $_SESSION['oauth_access_key_secret_trip'];

	// Create an OAuth Credential Object
	$oauth_cred = new OAuthConsumerCredential( $oauth_consumer_key, $oauth_consumer_secret, $oauth_access_token, $oauth_access_token_secret );

	// Create a new TripIt object
	$t = new TripIt( $oauth_cred, $api_url );

	$r = $t->list_trip();
	echo $r;
}
