<?php

include_once 'tripit.php';

session_id( 'uid' );
session_start();


if ( $_SESSION['oauth_request_key_trip'] == null ||
	$_SESSION['oauth_request_key_secret_trip'] == null ) {

	echo json_encode( array( 'status' => 'error'
			, 'msg' => 'missing request trip' ) );

}else {

	$api_url = 'https://api.tripit.com';

	$oauth_consumer_key = oauth_consumer_key_trip;
	$oauth_consumer_secret = oauth_consumer_key_secret_trip;
	$request_token = $_SESSION['oauth_request_key_trip'] ;
	$request_token_secret = $_SESSION['oauth_request_key_secret_trip'] ;

	$oauth_credential = new OAuthConsumerCredential( $oauth_consumer_key, $oauth_consumer_secret, $request_token, $request_token_secret );

	$tripit = new TripIt( $oauth_credential, $api_url );

	$keys = $tripit->get_access_token();
	$_SESSION['oauth_access_key_trip'] = $keys['oauth_token'];
	$_SESSION['oauth_access_key_secret_trip'] = $keys['oauth_token_secret'];
	if ( !empty( $keys['oauth_token'] ) ) {
		echo json_encode( array( 'status'=>'access',
				'msg' => 'granted access' ));
		}else {

			session_destroy();
			session_commit();

			echo json_encode( array( 'status'=>'fail',
					'msg' => 'with out keys' ) );
		}

	}










	
