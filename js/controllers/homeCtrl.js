'use strict'

app.controller('homeCtrl', function($scope, $location,XHR, AuthTrip, Trip, loginService) {
    $scope.usuario = {
        "foto": "",
        "nombre": ""
    };
    $scope.viajes = [];
    $scope.viajeactual = '';
    $scope.actividades = [];
    $scope.busqueda = "";

    $scope.mapOptions = {};
    var coordenadas = {};

    $scope.goActividad= function(id){
        $location.path('/#/actividades/'+id);
    };

    var list = Trip.getTripList();

    list.then(function(response) {
        console.log(response.data);
        if (response.data.status == 'errorKeys') {
            var access = AuthTrip.access();

            access.then(function(response) {
                if (response.data.status == 'access') {
                    var list2 = Trip.getTripList();
                    list2.then(function(response) {
                        console.log(response);
                        $scope.viajes = response.data.Trip;
                        if (!Array.isArray($scope.viajes)) {
                            $scope.viajes = new Array();
                            $scope.viajes.push(response.data.Trip);
                        }

                        $scope.usuario.nombre = response.data.Profile.public_display_name;
                        $scope.usuario.foto = response.data.Profile.photo_url;
                    });
                }
            });
        } else {
            $scope.viajes = response.data.Trip;
            if (!Array.isArray($scope.viajes)) {
                $scope.viajes = new Array();
                $scope.viajes.push(response.data.Trip);
            }
            $scope.usuario.nombre = response.data.Profile.public_display_name;
            $scope.usuario.foto = response.data.Profile.photo_url;
        }
    });





    $scope.salir = function() {
        loginService.logout();
    };

});