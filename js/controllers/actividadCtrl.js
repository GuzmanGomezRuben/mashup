app.controller('actividadCtrl', function($http, $scope, XHR, $timeout, $location, $routeParams, loginService) {
    var map;
    $scope.lugares_cercanos_ok = false;
    $scope.cambiarA = {
        "visible": "",
        "dato": ""
    };
    $scope.usuario = {
        "foto": "",
        "nombre": ""
    };
    var infowindow;
    var coordenadas;
    var markers = [];
    $scope.readyForMap = false;
    $scope.busqueda = {
        "mapa": false,
        "name": ""

    }
    $scope.informacion = {
        "titulo": "Hola que hace",
        "visible": false,
        "imagen": "",
        "data": "",
        "url": "",
        "visible": false

    };
    $scope.moneda = {
        "tipo": [],
        "cambio": []
    };
    $scope.lugares_cercanos = [];
    $scope.viajeactual = {
        "nombre": "",
        "lugar": ""
    };

    // Parámetro de id actividad
    var id = $routeParams.id_actividad;
    $scope.location = {
        "data": "17.0602487,-96.7496972",
        "tittle": "La casa de Jesus"
    };

    $scope.$on('mapInitialized', function(event, m) {
        map = m;
        infowindow = new google.maps.InfoWindow({
            content: 'Test2'
        });


    });

    $scope.salir = function() {
        loginService.logout();
    };

    function addMarker(i) {
        var latlng = new google.maps.LatLng($scope.lugares_cercanos[i].location.coordinate.latitude, $scope.lugares_cercanos[i].location.coordinate.longitude);

        markers[i] = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: 'images/geo.png',
            draggable: false,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(markers[i], 'click', (function(marker, i) {
            return function() {

                var txt = '<div>' + $scope.lugares_cercanos[i].name;

                if ($scope.lugares_cercanos[i].image_url) {
                    txt += '<center> <img src =' + $scope.lugares_cercanos[i].image_url +
                        '><img src =' + $scope.lugares_cercanos[i].rating_img_url + '></center>';
                } else {
                    txt += '<center><img src =' + $scope.lugares_cercanos[i].rating_img_url + '></center>';
                }


                txt += '</div>';
                infowindow.setContent(txt);
                infowindow.open(map, marker);
            }
        })(markers[i], i));
    }

    function deleteMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        this.markers = new Array();
    }




    $scope.cambios = [{
        "id": "AED",
        "nombre": "United Arab Emirates Dirham (AED)"
    }, {
        "id": "AFN",
        "nombre": "Afghan Afghani (AFN)"
    }, {
        "id": "ALL",
        "nombre": "Albanian Lek (ALL)"
    }, {
        "id": "AMD",
        "nombre": "Armenian Dram (AMD)"
    }, {
        "id": "ANG",
        "nombre": "Netherlands Antillean Guilder (ANG)"
    }, {
        "id": "AOA",
        "nombre": "Angolan Kwanza (AOA)"
    }, {
        "id": "ARS",
        "nombre": "Argentine Peso (ARS)"
    }, {
        "id": "AUD",
        "nombre": "Australian Dollar (A$)"
    }, {
        "id": "AWG",
        "nombre": "Aruban Florin (AWG)"
    }, {
        "id": "AZN",
        "nombre": "Azerbaijani Manat (AZN)"
    }, {
        "id": "BAM",
        "nombre": "Bosnia-Herzegovina Convertible Mark (BAM)"
    }, {
        "id": "BBD",
        "nombre": "Barbadian Dollar (BBD)"
    }, {
        "id": "BDT",
        "nombre": "Bangladeshi Taka (BDT)"
    }, {
        "id": "BGN",
        "nombre": "Bulgarian Lev (BGN)"
    }, {
        "id": "BHD",
        "nombre": "Bahraini Dinar (BHD)"
    }, {
        "id": "BIF",
        "nombre": "Burundian Franc (BIF)"
    }, {
        "id": "BMD",
        "nombre": "Bermudan Dollar (BMD)"
    }, {
        "id": "BND",
        "nombre": "Brunei Dollar (BND)"
    }, {
        "id": "BOB",
        "nombre": "Bolivian Boliviano (BOB)"
    }, {
        "id": "BRL",
        "nombre": "Brazilian Real (R$)"
    }, {
        "id": "BSD",
        "nombre": "Bahamian Dollar (BSD)"
    }, {
        "id": "BTC",
        "nombre": "Bitcoin (฿)"
    }, {
        "id": "BTN",
        "nombre": "Bhutanese Ngultrum (BTN)"
    }, {
        "id": "BWP",
        "nombre": "Botswanan Pula (BWP)"
    }, {
        "id": "BYR",
        "nombre": "Belarusian Ruble (BYR)"
    }, {
        "id": "BZD",
        "nombre": "Belize Dollar (BZD)"
    }, {
        "id": "CAD",
        "nombre": "Canadian Dollar (CA$)"
    }, {
        "id": "CDF",
        "nombre": "Congolese Franc (CDF)"
    }, {
        "id": "CHF",
        "nombre": "Swiss Franc (CHF)"
    }, {
        "id": "CLF",
        "nombre": "Chilean Unit of Account (UF) (CLF)"
    }, {
        "id": "CLP",
        "nombre": "Chilean Peso (CLP)"
    }, {
        "id": "CNH",
        "nombre": "CNH (CNH)"
    }, {
        "id": "CNY",
        "nombre": "Chinese Yuan (CN¥)"
    }, {
        "id": "COP",
        "nombre": "Colombian Peso (COP)"
    }, {
        "id": "CRC",
        "nombre": "Costa Rican Colón (CRC)"
    }, {
        "id": "CUP",
        "nombre": "Cuban Peso (CUP)"
    }, {
        "id": "CVE",
        "nombre": "Cape Verdean Escudo (CVE)"
    }, {
        "id": "CZK",
        "nombre": "Czech Republic Koruna (CZK)"
    }, {
        "id": "DEM",
        "nombre": "German Mark (DEM)"
    }, {
        "id": "DJF",
        "nombre": "Djiboutian Franc (DJF)"
    }, {
        "id": "DKK",
        "nombre": "Danish Krone (DKK)"
    }, {
        "id": "DOP",
        "nombre": "Dominican Peso (DOP)"
    }, {
        "id": "DZD",
        "nombre": "Algerian Dinar (DZD)"
    }, {
        "id": "EGP",
        "nombre": "Egyptian Pound (EGP)"
    }, {
        "id": "ERN",
        "nombre": "Eritrean Nakfa (ERN)"
    }, {
        "id": "ETB",
        "nombre": "Ethiopian Birr (ETB)"
    }, {
        "id": "EUR",
        "nombre": "Euro (€)"
    }, {
        "id": "FIM",
        "nombre": "Finnish Markka (FIM)"
    }, {
        "id": "FJD",
        "nombre": "Fijian Dollar (FJD)"
    }, {
        "id": "FKP",
        "nombre": "Falkland Islands Pound (FKP)"
    }, {
        "id": "FRF",
        "nombre": "French Franc (FRF)"
    }, {
        "id": "GBP",
        "nombre": "British Pound (£)"
    }, {
        "id": "GEL",
        "nombre": "Georgian Lari (GEL)"
    }, {
        "id": "GHS",
        "nombre": "Ghanaian Cedi (GHS)"
    }, {
        "id": "GIP",
        "nombre": "Gibraltar Pound (GIP)"
    }, {
        "id": "GMD",
        "nombre": "Gambian Dalasi (GMD)"
    }, {
        "id": "GNF",
        "nombre": "Guinean Franc (GNF)"
    }, {
        "id": "GTQ",
        "nombre": "Guatemalan Quetzal (GTQ)"
    }, {
        "id": "GYD",
        "nombre": "Guyanaese Dollar (GYD)"
    }, {
        "id": "HKD",
        "nombre": "Hong Kong Dollar (HK$)"
    }, {
        "id": "HNL",
        "nombre": "Honduran Lempira (HNL)"
    }, {
        "id": "HRK",
        "nombre": "Croatian Kuna (HRK)"
    }, {
        "id": "HTG",
        "nombre": "Haitian Gourde (HTG)"
    }, {
        "id": "HUF",
        "nombre": "Hungarian Forint (HUF)"
    }, {
        "id": "IDR",
        "nombre": "Indonesian Rupiah (IDR)"
    }, {
        "id": "IEP",
        "nombre": "Irish Pound (IEP)"
    }, {
        "id": "ILS",
        "nombre": "Israeli New Sheqel (₪)"
    }, {
        "id": "INR",
        "nombre": "Indian Rupee (Rs.)"
    }, {
        "id": "IQD",
        "nombre": "Iraqi Dinar (IQD)"
    }, {
        "id": "IRR",
        "nombre": "Iranian Rial (IRR)"
    }, {
        "id": "ISK",
        "nombre": "Icelandic Króna (ISK)"
    }, {
        "id": "ITL",
        "nombre": "Italian Lira (ITL)"
    }, {
        "id": "JMD",
        "nombre": "Jamaican Dollar (JMD)"
    }, {
        "id": "JOD",
        "nombre": "Jordanian Dinar (JOD)"
    }, {
        "id": "JPY",
        "nombre": "Japanese Yen (¥)"
    }, {
        "id": "KES",
        "nombre": "Kenyan Shilling (KES)"
    }, {
        "id": "KGS",
        "nombre": "Kyrgystani Som (KGS)"
    }, {
        "id": "KHR",
        "nombre": "Cambodian Riel (KHR)"
    }, {
        "id": "KMF",
        "nombre": "Comorian Franc (KMF)"
    }, {
        "id": "KPW",
        "nombre": "North Korean Won (KPW)"
    }, {
        "id": "KRW",
        "nombre": "South Korean Won (₩)"
    }, {
        "id": "KWD",
        "nombre": "Kuwaiti Dinar (KWD)"
    }, {
        "id": "KYD",
        "nombre": "Cayman Islands Dollar (KYD)"
    }, {
        "id": "KZT",
        "nombre": "Kazakhstani Tenge (KZT)"
    }, {
        "id": "LAK",
        "nombre": "Laotian Kip (LAK)"
    }, {
        "id": "LBP",
        "nombre": "Lebanese Pound (LBP)"
    }, {
        "id": "LKR",
        "nombre": "Sri Lankan Rupee (LKR)"
    }, {
        "id": "LRD",
        "nombre": "Liberian Dollar (LRD)"
    }, {
        "id": "LSL",
        "nombre": "Lesotho Loti (LSL)"
    }, {
        "id": "LTL",
        "nombre": "Lithuanian Litas (LTL)"
    }, {
        "id": "LVL",
        "nombre": "Latvian Lats (LVL)"
    }, {
        "id": "LYD",
        "nombre": "Libyan Dinar (LYD)"
    }, {
        "id": "MAD",
        "nombre": "Moroccan Dirham (MAD)"
    }, {
        "id": "MDL",
        "nombre": "Moldovan Leu (MDL)"
    }, {
        "id": "MGA",
        "nombre": "Malagasy Ariary (MGA)"
    }, {
        "id": "MKD",
        "nombre": "Macedonian Denar (MKD)"
    }, {
        "id": "MMK",
        "nombre": "Myanmar Kyat (MMK)"
    }, {
        "id": "MNT",
        "nombre": "Mongolian Tugrik (MNT)"
    }, {
        "id": "MOP",
        "nombre": "Macanese Pataca (MOP)"
    }, {
        "id": "MRO",
        "nombre": "Mauritanian Ouguiya (MRO)"
    }, {
        "id": "MUR",
        "nombre": "Mauritian Rupee (MUR)"
    }, {
        "id": "MVR",
        "nombre": "Maldivian Rufiyaa (MVR)"
    }, {
        "id": "MWK",
        "nombre": "Malawian Kwacha (MWK)"
    }, {
        "id": "MXN",
        "nombre": "Mexican Peso (MX$)"
    }, {
        "id": "MYR",
        "nombre": "Malaysian Ringgit (MYR)"
    }, {
        "id": "MZN",
        "nombre": "Mozambican Metical (MZN)"
    }, {
        "id": "NAD",
        "nombre": "Namibian Dollar (NAD)"
    }, {
        "id": "NGN",
        "nombre": "Nigerian Naira (NGN)"
    }, {
        "id": "NIO",
        "nombre": "Nicaraguan Córdoba (NIO)"
    }, {
        "id": "NOK",
        "nombre": "Norwegian Krone (NOK)"
    }, {
        "id": "NPR",
        "nombre": "Nepalese Rupee (NPR)"
    }, {
        "id": "NZD",
        "nombre": "New Zealand Dollar (NZ$)"
    }, {
        "id": "OMR",
        "nombre": "Omani Rial (OMR)"
    }, {
        "id": "PAB",
        "nombre": "Panamanian Balboa (PAB)"
    }, {
        "id": "PEN",
        "nombre": "Peruvian Nuevo Sol (PEN)"
    }, {
        "id": "PGK",
        "nombre": "Papua New Guinean Kina (PGK)"
    }, {
        "id": "PHP",
        "nombre": "Philippine Peso (Php)"
    }, {
        "id": "PKG",
        "nombre": "PKG (PKG)"
    }, {
        "id": "PKR",
        "nombre": "Pakistani Rupee (PKR)"
    }, {
        "id": "PLN",
        "nombre": "Polish Zloty (PLN)"
    }, {
        "id": "PYG",
        "nombre": "Paraguayan Guarani (PYG)"
    }, {
        "id": "QAR",
        "nombre": "Qatari Rial (QAR)"
    }, {
        "id": "RON",
        "nombre": "Romanian Leu (RON)"
    }, {
        "id": "RSD",
        "nombre": "Serbian Dinar (RSD)"
    }, {
        "id": "RUB",
        "nombre": "Russian Ruble (RUB)"
    }, {
        "id": "RWF",
        "nombre": "Rwandan Franc (RWF)"
    }, {
        "id": "SAR",
        "nombre": "Saudi Riyal (SAR)"
    }, {
        "id": "SBD",
        "nombre": "Solomon Islands Dollar (SBD)"
    }, {
        "id": "SCR",
        "nombre": "Seychellois Rupee (SCR)"
    }, {
        "id": "SDG",
        "nombre": "Sudanese Pound (SDG)"
    }, {
        "id": "SEK",
        "nombre": "Swedish Krona (SEK)"
    }, {
        "id": "SGD",
        "nombre": "Singapore Dollar (SGD)"
    }, {
        "id": "SHP",
        "nombre": "St. Helena Pound (SHP)"
    }, {
        "id": "SKK",
        "nombre": "Slovak Koruna (SKK)"
    }, {
        "id": "SLL",
        "nombre": "Sierra Leonean Leone (SLL)"
    }, {
        "id": "SOS",
        "nombre": "Somali Shilling (SOS)"
    }, {
        "id": "SRD",
        "nombre": "Surinamese Dollar (SRD)"
    }, {
        "id": "STD",
        "nombre": "São Tomé &amp; Príncipe Dobra (STD)"
    }, {
        "id": "SVC",
        "nombre": "Salvadoran Colón (SVC)"
    }, {
        "id": "SYP",
        "nombre": "Syrian Pound (SYP)"
    }, {
        "id": "SZL",
        "nombre": "Swazi Lilangeni (SZL)"
    }, {
        "id": "THB",
        "nombre": "Thai Baht (THB)"
    }, {
        "id": "TJS",
        "nombre": "Tajikistani Somoni (TJS)"
    }, {
        "id": "TMT",
        "nombre": "Turkmenistani Manat (TMT)"
    }, {
        "id": "TND",
        "nombre": "Tunisian Dinar (TND)"
    }, {
        "id": "TOP",
        "nombre": "Tongan Paʻanga (TOP)"
    }, {
        "id": "TRY",
        "nombre": "Turkish Lira (TRY)"
    }, {
        "id": "TTD",
        "nombre": "Trinidad Tobago Dollar(TTD)"
    }, {
        "id": "TWD",
        "nombre": "New Taiwan Dollar (NT$)"
    }, {
        "id": "TZS",
        "nombre": "Tanzanian Shilling (TZS)"
    }, {
        "id": "UAH",
        "nombre": "Ukrainian Hryvnia (UAH)"
    }, {
        "id": "UGX",
        "nombre": "Ugandan Shilling (UGX)"
    }, {
        "id": "USD",
        "nombre": "US Dollar ($)"
    }, {
        "id": "UYU",
        "nombre": "Uruguayan Peso (UYU)"
    }, {
        "id": "UZS",
        "nombre": "Uzbekistani Som (UZS)"
    }, {
        "id": "VEF",
        "nombre": "Venezuelan Bolívar (VEF)"
    }, {
        "id": "VND",
        "nombre": "Vietnamese Dong (₫)"
    }, {
        "id": "VUV",
        "nombre": "Vanuatu Vatu (VUV)"
    }, {
        "id": "WST",
        "nombre": "Samoan Tala (WST)"
    }, {
        "id": "XAF",
        "nombre": "Central African CFA Franc (FCFA)"
    }, {
        "id": "XCD",
        "nombre": "East Caribbean Dollar (EC$)"
    }, {
        "id": "XDR",
        "nombre": "Special Drawing Rights (XDR)"
    }, {
        "id": "XOF",
        "nombre": "West African CFA Franc (CFA)"
    }, {
        "id": "XPF",
        "nombre": "CFP Franc (CFPF)"
    }, {
        "id": "YER",
        "nombre": "Yemeni Rial (YER)"
    }, {
        "id": "ZAR",
        "nombre": "South African Rand (ZAR)"
    }, {
        "id": "ZMK",
        "nombre": "Zambian Kwacha (1968–2012) (ZMK)"
    }, {
        "id": "ZMW",
        "nombre": "Zambian Kwacha (ZMW)"
    }, {
        "id": "ZWL",
        "nombre": "Zimbabwean Dollar (2009) (ZWL)"
    }];



    // Todas las actividadades de un viaje en especifico
    XHR.async({
        method: 'GET',
        url: "php/example.php?url=https://api.tripit.com/v1/get/trip/id/" + id + "/include_objects/true/format/json",
        headers: {
            'Accept': 'application/json'
        }
    }).then(
        function success(response) {

            // Cuando solo es una actividad envía un objeto
            // cuando son varias envia un arreglo ._.
            if (response.data.ActivityObject) {
                if (Array.isArray(response.data.ActivityObject)) {
                    $scope.actividades = response.data.ActivityObject;
                } else {
                    $scope.actividades = new Array();
                    $scope.actividades.push(response.data.ActivityObject);
                }

                console.log(response.data.ActivityObject);
                $scope.usuario.nombre = response.data.Profile.public_display_name;
                $scope.usuario.foto = response.data.Profile.photo_url;

                $scope.cont = 0;
                for (var i = 0; i < $scope.actividades.length; i++) {

                    $http.jsonp('https://api.flickr.com/services/feeds/photos_public.gne?tags=' + $scope.actividades[i].display_name + '&tagmode=any&format=json&').success(function(data) {

                    });
                    jsonFlickrFeed = function(data) {

                        $scope.actividades[$scope.cont++].image = data.items[0].media.m;
                        console.log($scope.actividades);
                    }
                }
            }
            $scope.viajeactual.nombre = response.data.Trip.display_name;
            $scope.viajeactual.lugar = response.data.Trip.primary_location;
            $scope.mostrarActividad = true;
            coordenadas = response.data.Trip.PrimaryLocationAddress;
            $scope.location.data = coordenadas.latitude + "," + coordenadas.longitude;
            $scope.location.tittle = response.data.Trip.display_name;
            console.log(response);
            $scope.readyForMap = true;

            $http({
                    method: "GET",
                    url: "php/yelp.php?term=comida,hoteles&latitud=" + coordenadas.latitude + "&longitud=" + coordenadas.longitud + "&lugar=" + coordenadas.address
                }).then(function(response) {
                    $scope.lugares_cercanos = response.data.businesses;
                    console.log(response);
                    console.log($scope.lugares_cercanos);

                    //vaciar marcadores

                    markers = [];
                    for (i = 0; i < $scope.lugares_cercanos.length; i++) {
                        $scope.lugares_cercanos[i].location.data = $scope.lugares_cercanos[i].location.coordinate.latitude + "," + $scope.lugares_cercanos[i].location.coordinate.longitude;

                        addMarker(i);


                    }
                    $scope.lugares_cercanos_ok = true;

                }),
                function(error) {
                    console.log(error);
                };
        },
        function error(response) {
            $scope.mostrarActividad = false
            console.log(response);
        });

    $scope.informacion = function(a, b, c) {
        console.log(a);
        console.log(b);
        console.log(c);

        marker = new google.maps.Marker({
            position: {
                lat: 20.961439,
                lng: -89.65
            },
            map: map,
            title: 'Uluru (Ayers Rock)'
        });
        infowindow.open(map, marker);

        /*
        $scope.informacion.visible = true;
        $scope.informacion.data = $scope.lugares_cercanos[c].location.data;
        $scope.informacion.titulo = $scope.lugares_cercanos[c].name;
        $scope.informacion.url = $scope.lugares_cercanos[c].image_url;
        $scope.informacion.visible = true;
        */




    }




    $scope.compartir = function() {
        var url_image = $scope.actividades[0].image;
        var titulo = $scope.viajeactual.nombre;
        var lugar = $scope.viajeactual.lugar;
        console.log("Compartiendo");
        $http({
                method: "GET",
                url: "php/iniciar.php?url=" + url_image + "&titulo=" + titulo + "-" +
                    lugar
            }).then(function(response) {
                console.log(response);
                $timeout(function() {
                    $scope.compartido = true;
                    // Loadind done here - Show message for 3 more seconds.
                    $timeout(function() {
                        $scope.compartido = false;
                    }, 3000);

                }, 2000);

            }),
            function(error) {
                console.log(error);
            };
    }


    $scope.masLugares = function() {
        console.log(map);
        if ($scope.busqueda.name) {
            $scope.busqueda.mapa = true;
            $http({
                    method: "GET",
                    url: "php/yelp.php?term=" + $scope.busqueda.name + "&latitud=" + coordenadas.latitude + "&longitud=" + coordenadas.longitud + "&lugar=" + coordenadas.address
                }).then(function(response) {
                    $scope.lugares_cercanos = response.data.businesses;
                    console.log(response);
                    console.log($scope.lugares_cercanos);

                    deleteMarkers();
                    for (i = 0; i < $scope.lugares_cercanos.length; i++) {
                        $scope.lugares_cercanos[i].location.data = $scope.lugares_cercanos[i].location.coordinate.latitude + "," + $scope.lugares_cercanos[i].location.coordinate.longitude;
                        addMarker(i);
                    }
                    $scope.lugares_cercanos_ok = true;
                    $scope.busqueda.mapa = false;

                }),
                function(error) {
                    console.log(error);
                };
            console.log("Mas lugares: " + $scope.busqueda);
        }

    }

    // Función para cambiar la moneda
    $scope.cambioMoneda = function() {
        console.log($scope.cambiarA);
        angular.forEach($scope.actividades, function(value, key) {
            if ($scope.actividades[key].total_cost && $scope.actividades[key].total_cost > 0) {
                XHR.async({
                    method: 'POST',
                    url: "php/monedas.php?moneda_origen=MXN&moneda_destino=" + $scope.cambiarA.dato +
                        "&cantidad=" + $scope.actividades[key].total_cost,
                    headers: {
                        'Accept': 'application/json'
                    }
                }).then(function(response) {
                    console.log(response.data);
                    $scope.actividades[key]["cambio"] = response.data;

                });

            } else if ($scope.actividades[key].total_cost && $scope.actividades[key].total_cost == 0) {
                $scope.actividades[key]["cambio"] = 0;
            }
        });






    }


});