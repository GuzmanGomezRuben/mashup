'use strict';


app.controller('loginCtrl', function($scope, AuthTrip, $window, $location,loginService) {

    var domain =  $location.absUrl();
    console.log(domain);

    var logged = loginService.isLogged();

    logged.then(function(response){
    	if(response.data.status == 'authentified'){
    		$location.path('/#/home');
    	}    	
    });
    	
    

    $scope.comenzar = function() {

        var permission = AuthTrip.permission();
        permission.then(function(response) {
            console.log(response);

            $window.location.href = 'https://www.tripit.com/oauth/authorize?oauth_token=' +
                response.data.oauth_token + '&oauth_callback=' + domain ;


        });

    };

});