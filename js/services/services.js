app.factory('XHR', function($http) {
    return {
        async: function(req) {

            return $http(req);

        }
    };
});

app.factory('Trip', function($http) {
    return {
        getTripList: function() {
            console.log("Enter get List Trip");
            var promise = $http.get('php/example.php?url=https://api.tripit.com/v1/list/trip/format/json');
            return promise;
        }
    }
});

app.factory('Session', function($http) {
    return {
        set: function(key, value) {
            return sessionStorage.setItem(key, value);
        },
        get: function(key) {
            return sessionStorage.getItem(key);
        },
        destroy: function(key) {
            $http.post('/php/destroy_session.php');
            return sessionStorage.removeItem(key)
        }
    }
});

app.factory('AuthTrip', function($http) {
    return {
        permission: function() {
            console.log("Enter persmision function");
            var promise = $http.get('php/permissionTrip.php');
            return promise;
        },
        access: function() {
            console.log("Enter acces funcion");
            var promise = $http.get('php/accessTrip.php');
            return promise;
        }

    }

});

app.factory('loginService', function($http, Session, $location) {
    return {
        login: function(value) {
            Session.set('user', value);
        },
        logout: function() {
            $http.post('/php/destroy_session.php');
            Session.destroy('user');
            $location.path('/login');
        },
        isLogged: function() {
            var $check = $http.post('/php/check_session.php');
            return $check;
            /*
            if (Session.get('user')) {
                return true
            }
            */
        }
    }
});