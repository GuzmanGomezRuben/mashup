var app = angular.module('viajes', ['ngRoute', 'ngMap']);

app.config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'templates/tab-login.html',
                    controller: 'loginCtrl'
                })
                .when('/#/home', {
                    templateUrl: 'templates/tab-main.html',
                    controller: 'homeCtrl'
                })
                .when('/#/acercade', {
                    templateUrl: 'templates/tab-about.html'

                })
                .when('/#/actividades/:id_actividad', {
                    templateUrl: 'templates/tab-actividades.html',
                    controller: 'actividadCtrl'

                })
                .otherwise('/');

            $locationProvider.html5Mode(true);
        }
    ])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }]);


// Prevenir el acesso a quienes no han iniciado sesion con tripit
app.run(function($rootScope, $location, loginService){
    var routesPermision = ['/home', '/actividades/'];

    $rootScope.$on('$routeChangeStart', function() {
        if (routesPermision.indexOf($location.path()) != -1 ) {
            var connected = loginService.isLogged();
            connected.then(function(response){
                if(response.data.status == 'failed'){
                    $location.path('/login');
                }
            });
            
        }
    });

});
